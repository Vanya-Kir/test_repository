from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Post


class HomePageView(ListView):
    template_name = 'home.htm'
    model = Post


class BlogDetailView(DetailView):
    template_name = 'post_detail.htm'
    model = Post


class BlogCreateView(CreateView):
    model = Post
    template_name = 'post_new.htm'
    fields = '__all__'


class BlogUpdaateView(UpdateView):
    model = Post
    fields = ['title', 'body']
    template_name = 'post_edit.htm'


class BlogDeleteView(DeleteView):
    model = Post
    template_name = 'post_delete.htm'
    success_url = reverse_lazy('home')